var express = require('express');
var path = require('path');
var http = require('http');
var fs = require('fs');
var config = require(__dirname + '/config.js');
var app = express();
var exec = require('child_process').exec;
var netmask = require('netmask').Netmask;
var authorizedSubnet = config.security.authorizedSubnet.map(function (subnet) {
    return new netmask(subnet);
});
var inAuthorizedSubnet = function (ip) {
    return authorizedSubnet.some(function (subnet) {
        return subnet.contains(ip);
    });
};
app.set('server_port', config.server.port);
app.get('/', function (req, res) {
    res.json({status: 'ok'});
});
app.post('/deploy', function (req, res) {
    var authorizedIps = config.security.authorizedIps;
    var line = config.action.exec;
    var from = req.ip.replace("::ffff:", "");
    console.log('Webhook from:', from);

    if ((inAuthorizedSubnet(from) || authorizedIps.indexOf(from) >= 0)) {
        exec(line, function (error, stdout, stderr) {
            if(stderr) console.log('Webhook stderr:', error);
            if (error !== null) {
                console.log('Webhook exec error:', error);
                res.status(403).send({error:true, error_log: error});
            } else {
                console.log('Webhook exec:', stdout);
                res.status(200).send({error:false, response: stdout});
            }
        });
    } else {
        res.status(403).end();
    }
});
!process.env["NODE_ENV"] && (process.env["NODE_ENV"] = config.environment);
http.createServer(app).listen(app.get('server_port'), function () {
    console.log("Initializing server. ENV = ", process.env["NODE_ENV"]);
    console.log("Deployment server listening on port:", app.get('server_port'));
});
