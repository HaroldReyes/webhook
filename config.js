module.exports = {
    environment: "development",
    server: {port: "61440"},
    security: {
        authorizedIps: [
            '::1',
            '127.0.0.1',
            'localhost'
        ],
        authorizedSubnet: [
            '104.192.143.0/24',
            '192.30.252.0/22',
            '185.199.108.0/22'

        ]
    },
    repository: {
        branch: 'refs/heads/master'
    },
    action: {
        exec: "/home/ubuntu/webhook/deploy.sh"
    }
};
