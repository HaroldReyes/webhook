#!/bin/bash
cd /home/ubuntu/server/

echo "Updating Code"
echo "====================================="
git fetch
git pull

echo "Stopping server"
echo "====================================="
forever stop app.js

echo "Installing Dependencies"
echo "====================================="
npm install

echo "Database Migration"
echo "====================================="
sequelize db:migrate

echo "Generate Documentation"
echo "====================================="
npm run docs

echo "Restart server"
echo "====================================="
forever start app.js
